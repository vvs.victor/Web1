CREATE DATABASE dbweb;
USE dbweb;
/* Creación tabla con los usuarios*/
/* Password in SHA256 -> 64 chars */
CREATE TABLE users(
  id int not null AUTO_INCREMENT,
  user varchar(255),
  passwd char(64),
  name varchar(255),
  surname varchar(255),
  primary key(id)
  ) ENGINE=InnoDB;

/* Password is admin in SHA256*/
INSERT INTO users (user, passwd, name, surname) VALUES ("admin","8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918","Victor","Moreno");
