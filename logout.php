<?php
  session_start();
  // Destrucción de las sesiones
  if(session_destroy()){
    header("Location: indexlogin.php"); // Redirección a la página principal
  }
?>
